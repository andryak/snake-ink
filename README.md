# Snake Ink

This project is an implementation of the popular Snake game for the
command line using React Ink.

![](./snake-ink.gif)

# Build and run

To install project dependencies run `yarn install`.
To build and run the project use `yarn start`.
