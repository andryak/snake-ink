import {useState} from 'react';

export default () => {
  const [, setState] = useState(false);
  return () => setState(prevState => !prevState);
};
