export default (p1, p2) => (p1.x === p2.x) && (p1.y === p2.y);
