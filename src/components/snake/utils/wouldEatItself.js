import moveTo from './moveTo';
import coincident from './coincident';

export default (arenaSize, snake, direction) => {
  const size = snake.length;
  if (size < 2) {
    return false;
  }
  const head = snake[size - 1];
  const neck = snake[size - 2];
  return coincident(moveTo(head, direction, arenaSize), neck);
};
