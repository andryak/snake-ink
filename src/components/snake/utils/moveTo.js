import Direction from '../Direction';

export default ({x, y}, direction, arenaSize) => {
  let dx = 0;
  let dy = 0;
  switch (direction) {
    case Direction.NONE:
      break;
    case Direction.UP:
      dy = -1;
      break;
    case Direction.DOWN:
      dy = 1;
      break;
    case Direction.LEFT:
      dx = -1;
      break;
    case Direction.RIGHT:
      dx = 1;
      break;
  }
  const coords = {
    x: x + dx,
    y: y + dy,
  };
  if (coords.x < 0) {
    coords.x += arenaSize;
  }
  if (coords.x >= arenaSize) {
    coords.x -= arenaSize;
  }
  if (coords.y < 0) {
    coords.y += arenaSize;
  }
  if (coords.y >= arenaSize) {
    coords.y -= arenaSize;
  }
  return coords;
};
