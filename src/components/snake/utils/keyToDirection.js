import Direction from '../Direction';

const ARROW_UP = '\u001B[A';
const ARROW_DOWN = '\u001B[B';
const ARROW_LEFT = '\u001B[D';
const ARROW_RIGHT = '\u001B[C';

export default key => {
  const keyChar = String(key);
  switch (keyChar) {
    case ARROW_UP:
      return Direction.UP;
    case ARROW_DOWN:
      return Direction.DOWN;
    case ARROW_LEFT:
      return Direction.LEFT;
    case ARROW_RIGHT:
      return Direction.RIGHT;
  }
};
