import React from 'react';
import {Box} from 'ink';
import chalk from 'chalk';
import last from 'lodash/last';
import range from 'lodash/range';
import repeat from 'lodash/repeat';
import initial from 'lodash/initial';
import Status from './Status';


const computeMatrix = (size, cellSize, state) => {
  const cell = repeat(' ', cellSize);

  // Init the matrix with black cells.
  let matrix = range(size).map(
    () => range(size).map(
      () => chalk.bgBlack(cell)));

  // Paint the food in magenta.
  if (state.food !== undefined) {
    const {x, y} = state.food.coords;
    matrix[y][x] = chalk.bgMagenta(cell);
  }

  // Paint the snake body and then the head in a brighter color.
  initial(state.snake.body).forEach(({x, y}) => {
    matrix[y][x] = (state.snake.status === Status.ALIVE)
      ? chalk.bgGreen(cell)
      : chalk.bgRed(cell);
  });
  const {x, y} = last(state.snake.body);
  matrix[y][x] = (state.snake.status === Status.ALIVE)
    ? chalk.bgGreenBright(cell)
    : chalk.bgRedBright(cell);

  // Paint white borders around the arena.
  const whiteCell = chalk.bgWhite(cell);
  const whiteLine = range(size + 2).map(() => whiteCell);
  matrix = [
    whiteLine,
    ...matrix.map(row => [
      whiteCell,
      ...row,
      whiteCell
    ]),
    whiteLine,
  ];
  return matrix.map(row => row.join('')).join('\n')
};

const Arena = ({size, cellSize = 3, state}) => (
  <>
    <Box>Score: {state.score}</Box>
    <Box>{computeMatrix(size, cellSize, state)}</Box>
  </>
);

export default Arena;
